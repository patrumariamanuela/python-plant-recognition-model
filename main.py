import collections
import io
import math
import os
import random
from typing import Union, List, Tuple

from pyasn1.compat.octets import null
from six.moves import urllib
from tensorflow import compat

from IPython.display import clear_output, Image, display, HTML

import tensorflow as tf

"""tf.disable_v2_behavior()"""

import tensorflow_hub as hub

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn.metrics as sk_metrics
import time
import tarfile
import os
from collections import OrderedDict

FLOWERS_DIR = './flower_photos'
TRAIN_FRACTION = 0.8
RANDOM_SEED = 2018


def download_images():
    """If the images aren't already downloaded, save them to FLOWERS_DIR."""
    if not os.path.exists(FLOWERS_DIR):
        DOWNLOAD_URL = 'http://download.tensorflow.org/example_images/flower_photos.tgz'
        print('Downloading flower images from %s...' % DOWNLOAD_URL)
        urllib.request.urlretrieve(DOWNLOAD_URL, 'flower_photos.tgz')
        """extragere fisier tgz"""
        tar = tarfile.open("flower_photos.tgz")
        tar.extractall()
        tar.close()
    print('Flower photos are located in %s' % FLOWERS_DIR)


def make_train_and_test_sets():
    """Split the data into train and test sets and get the label classes."""
    global label_to_class
    train_examples, test_examples = [], []
    """initializare classes (collection)"""
    classes = {}
    shuffler = random.Random(RANDOM_SEED)
    is_root = True
    """tf.compat.v1.io.gfile.walk pt versiune"""
    for (dirname, subdirs, filenames) in tf.compat.v1.io.gfile.walk(FLOWERS_DIR):
        # The root directory gives us the classes
        if is_root:
            subdirs = sorted(subdirs)
            classes = collections.OrderedDict(enumerate(subdirs))
            print("kkk: ", collections.OrderedDict(enumerate(subdirs)))
            label_to_class = dict([(x, i) for i, x in enumerate(subdirs)])
            is_root = False
        # The sub directories give us the image files for training.
        else:
            filenames.sort()
            shuffler.shuffle(filenames)
            full_filenames = [os.path.join(dirname, f) for f in filenames]
            label = os.path.split(dirname)[-1]#dirname.split(r'/')[-1]
            """facut de mine, pentru a scapa de eroarea cu keyError double slash"""
            # path = label
            # path = os.path.normpath(label)
            # path.split(os.sep)
            # print("jjjj", path)
            label_class = label_to_class[label]
            # An example is the image file and it's label class.
            examples = list(zip(full_filenames, [label_class] * len(filenames)))
            num_train = int(len(filenames) * TRAIN_FRACTION)
            train_examples.extend(examples[:num_train])
            test_examples.extend(examples[num_train:])

    shuffler.shuffle(train_examples)
    shuffler.shuffle(test_examples)
    return train_examples, test_examples, classes


# Download the images and split the images into train and test sets.
download_images()

TRAIN_EXAMPLES, TEST_EXAMPLES, CLASSES = make_train_and_test_sets()
print(CLASSES)
NUM_CLASSES = len(CLASSES)

print('\nThe dataset has %d label classes: %s' % (NUM_CLASSES, CLASSES.values()))
print('There are %d training images' % len(TRAIN_EXAMPLES))
print('There are %d test images' % len(TEST_EXAMPLES))




