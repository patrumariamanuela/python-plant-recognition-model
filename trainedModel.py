import random

import main
import model
import exploreData
import tensorflow as tf

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn.metrics as sk_metrics


# How long will we train the network (number of batches).
NUM_TRAIN_STEPS = 100
# How many training examples we use in each step.
TRAIN_BATCH_SIZE = 10
# How often to evaluate the model performance.
EVAL_EVERY = 10

tf.compat.v1.disable_eager_execution()


def get_batch(batch_size=None, test=False):
    """Get a random batch of examples."""
    examples = main.TEST_EXAMPLES if test else main.TRAIN_EXAMPLES
    batch_examples = random.sample(examples, batch_size) if batch_size else examples
    return batch_examples


def get_images_and_labels(batch_examples):
    images = [exploreData.get_encoded_image(e) for e in batch_examples]
    one_hot_labels = [get_label_one_hot(e) for e in batch_examples]
    return images, one_hot_labels


def get_label_one_hot(example):
    """Get the one hot encoding vector for the example."""
    one_hot_vector = np.zeros(main.NUM_CLASSES)
    np.put(one_hot_vector, exploreData.get_label(example), 1)
    return one_hot_vector


with tf.compat.v1.Session() as sess:
    sess.run(tf.compat.v1.global_variables_initializer())
    for i in range(NUM_TRAIN_STEPS):
        # Get a random batch of training examples.
        train_batch = get_batch(batch_size=TRAIN_BATCH_SIZE)
        batch_images, batch_labels = get_images_and_labels(train_batch)
        # Run the train_op to train the model.
        train_loss, _, train_accuracy = sess.run(
            [model.cross_entropy_mean, model.train_op, model.accuracy],
            feed_dict={model.encoded_images: batch_images, model.labels: batch_labels})
        is_final_step = (i == (NUM_TRAIN_STEPS - 1))
        if i % EVAL_EVERY == 0 or is_final_step:
            # Get a batch of test examples.
            test_batch = get_batch(batch_size=None, test=True)
            batch_images, batch_labels = get_images_and_labels(test_batch)
            # Evaluate how well our model performs on the test set.
            test_loss, test_accuracy, test_prediction, correct_predicate = sess.run(
                [model.cross_entropy_mean, model.accuracy, model.prediction, model.correct_prediction],
                feed_dict={model.encoded_images: batch_images, model.labels: batch_labels})
            print('Test accuracy at step %s: %.2f%%' % (i, (test_accuracy * 100)))


            def show_confusion_matrix(test_labels, predictions):
                """Compute confusion matrix and normalize."""
                confusion = sk_metrics.confusion_matrix(
                    np.argmax(test_labels, axis=1), predictions)
                confusion_normalized = confusion.astype("float") / confusion.sum(axis=1)
                axis_labels = list(main.CLASSES.values())
                ax = sns.heatmap(
                    confusion_normalized, xticklabels=axis_labels, yticklabels=axis_labels,
                    cmap='Blues', annot=True, fmt='.2f', square=True)
                plt.title("Confusion matrix")
                plt.ylabel("True label")
                plt.xlabel("Predicted label")
                plt.show()


            show_confusion_matrix(batch_labels, test_prediction)

            incorrect = [
                (example, main.CLASSES[prediction])
                for example, prediction, is_correct in zip(test_batch, test_prediction, correct_predicate)
                if not is_correct
            ]
            exploreData.display_images(
                [(exploreData.get_image(example), "prediction: {0}\nlabel:{1}".format(incorrect_prediction, exploreData.get_class(example)))
                 for (example, incorrect_prediction) in incorrect[:20]])
