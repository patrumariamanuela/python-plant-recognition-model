import collections
import io
import math
import os
import random
from typing import Union

import path as path
from pyasn1.compat.octets import null
from six.moves import urllib
from tensorflow import compat

from IPython.display import clear_output, Image, display, HTML

import tensorflow as tf

"""tf.disable_v2_behavior()"""

import tensorflow_hub as hub
import main
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn.metrics as sk_metrics
import time
import os
import re


# def get_label(path):
#     image_path = os.path.join(path)
#     """Get the label (number) for given example."""
#     return image_path.replace(r"\\", "\\")
#
#
# def get_class(path):
#     """Get the class (string) of given example."""
#     image_path = path[1].replace(r"\\", "\\")
#     return main.CLASSES[get_label(image_path)]
#
#
# def get_encoded_image(path):
#     """Get the image data (encoded jpg) of given example."""
#     image_path = os.path.join(path)
#     return tf.io.gfile.GFile(image_path, 'rb').read()
#
#
# def get_image(path):
#     """Get image as np.array of pixels for given example."""
#     image_path = path[0].replace(r"\\", "\\")
#     return plt.imread(io.BytesIO(get_encoded_image(image_path)), format='jpg')
#
#
# def display_images(images_and_classes, cols=5):
#     """Display given images and their labels in a grid."""
#     rows = int(math.ceil(len(images_and_classes) / cols))
#     fig = plt.figure()
#     fig.set_size_inches(cols * 3, rows * 3)
#     for i, (image, flower_class) in enumerate(images_and_classes):
#         plt.subplot(rows, cols, i + 1)
#         plt.axis('off')
#         plt.imshow(image)
#         plt.title(flower_class)
#
#
# NUM_IMAGES = 15
# for path in main.TRAIN_EXAMPLES[:NUM_IMAGES:]:
#     print("llllllllll" , path)
# display_images([(get_image(path), get_class(path))
#                 for path in main.TRAIN_EXAMPLES[:NUM_IMAGES:]])

def get_label(example):
    """Get the label (number) for given example."""
    return example[1]


def get_class(example):
    """Get the class (string) of given example."""
    return main.CLASSES[get_label(example)]


def get_encoded_image(example):
    """Get the image data (encoded jpg) of given example."""
    image_path = example[0]
    return tf.io.gfile.GFile(image_path, 'rb').read()


def get_image(example):
    """Get image as np.array of pixels for given example."""
    return plt.imread(io.BytesIO(get_encoded_image(example)), format='jpg')


def display_images(images_and_classes, cols=5):
    """Display given images and their labels in a grid."""
    rows = int(math.ceil(len(images_and_classes) / cols))
    fig = plt.figure()
    fig.set_size_inches(cols * 3, rows * 3)
    for i, (image, flower_class) in enumerate(images_and_classes):
        plt.subplot(rows, cols, i + 1)
        plt.axis('off')
        plt.imshow(image)
        plt.title(flower_class)
        #plt.show()


NUM_IMAGES = 15
display_images([(get_image(example), get_class(example))
                for example in main.TRAIN_EXAMPLES[:NUM_IMAGES]])
